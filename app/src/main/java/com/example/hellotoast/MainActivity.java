package com.example.hellotoast;

import static android.graphics.Color.*;

import androidx.appcompat.app.AppCompatActivity;

import android.content.res.ColorStateList;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private int mCount = 0;
    private TextView mShowCount;
    private Button buttonCount;
    private Button buttonZero;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mShowCount = findViewById(R.id.show_count);
        buttonCount = findViewById(R.id.button_count);
        buttonZero = findViewById(R.id.zero);
        if (savedInstanceState != null) {
            mCount = savedInstanceState.getInt("counter");
            mShowCount.setText(String.valueOf(mCount));
        }
        setButtonsColor();
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("counter", mCount);
    }

    public void showToast(View view) {
        Toast.makeText(this, R.string.toast_message, Toast.LENGTH_SHORT).show();
    }

    public void countUp(View view) {
        mCount++;
        if (mShowCount != null) {
            mShowCount.setText(Integer.toString(mCount));
            setButtonsColor();
        }
    }

    public void setZero(View view) {
        mCount = 0;
        if (mShowCount != null) {
            mShowCount.setText(Integer.toString(mCount));
            setButtonsColor();
        }
    }

    private void setButtonsColor() {
        int buttonZeroColor;
        int buttonCountColor;

        if (mCount == 0) {
            buttonZeroColor = parseColor(Color.grey);
            buttonCountColor = parseColor(Color.blue);
        } else if (mCount % 2 == 0) {
            buttonZeroColor = parseColor(Color.pink);
            buttonCountColor = parseColor(Color.blue);
        } else {
            buttonZeroColor = parseColor(Color.pink);
            buttonCountColor = parseColor(Color.green);
        }

        buttonZero.setBackgroundTintList(ColorStateList.valueOf(buttonZeroColor));
        buttonCount.setBackgroundTintList(ColorStateList.valueOf(buttonCountColor));
    }

    private static class Color {
        static String blue = "#0000F5";
        static String grey = "#AAAAAA";
        static String green = "#75FB4C";
        static String pink = "#EA33F7";
    }
}